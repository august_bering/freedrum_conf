package masonsoft.freedrumconf;

import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import freedrum.freedrumsdk.Drumstick;
import freedrum.freedrumsdk.DrumstickFoundCallback;
import freedrum.freedrumsdk.FreedrumController;

public class BLEScanActivitiy extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private static final String TAG = "freedrum_conf";
    private ListView deviceListView;
    private Button fab;

    int assignPad = -1;
    private FreedrumController freedrumController;
    final DrumstickFoundCallback callback = new DrumstickFoundCallback() {
        @Override
        public void stickFound(Drumstick drumstick) {

            mLeDeviceListAdapter.add(drumstick);
            mLeDeviceListAdapter.notifyDataSetChanged();
        }

        @Override
        public void scanFailed(int errorCode) {
            switch (errorCode){
                case SCAN_FAILED_ALREADY_STARTED:
                    Snackbar.make(deviceListView, "Already started, stopping.", Snackbar.LENGTH_LONG).show();
                    freedrumController.stopScan();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blescan_activitiy);
        deviceListView = (ListView) findViewById(R.id.devicelist);

        fab = (Button) findViewById(R.id.scan);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Starting scan", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                scanLeDevice(true);
            }
        });

        Button assignButton = (Button) findViewById(R.id.action_assign);
        assignButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Assigning pads. Click on each pad in the order you want them.", Snackbar.LENGTH_LONG).show();
                assignPad = 0;
            }
        });
        mLeDeviceListAdapter = new ArrayAdapter<Drumstick>(this, R.layout.ble_device_list_item, R.id.name);

    }

    @Override
    protected void onStart() {
        super.onStart();

        freedrumController = FreedrumController.instance(this);


//        final BluetoothManager bluetoothManager =
//                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
//        mBluetoothAdapter = bluetoothManager.getAdapter();
//        bluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
        mHandler = new Handler();


        deviceListView.setAdapter(mLeDeviceListAdapter);

        deviceListView.setOnItemClickListener(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        scanLeDevice(true);
    }

    private void scanLeDevice(boolean b) {


        if (b){
            int res=freedrumController.startScan(callback);

            switch (res){
                case 0://started scan
                    // Stops scanning after a pre-defined scan period.
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mScanning = false;
                            freedrumController.stopScan();
                            fab.setEnabled(true);
                        }
                    }, SCAN_PERIOD);

                    break;
                case -1:
                    //new permission required.
                    Snackbar.make(deviceListView,"Not started. No permission", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    break;
            }

        }
        else
            freedrumController.stopScan();
    }


    private static final long SCAN_PERIOD = 1000 * 60;
    private Handler mHandler;
    private boolean mScanning;

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults[0]==PackageManager.PERMISSION_GRANTED){
            scanLeDevice(true);
        }
    }


    final static ParcelUuid apple_midi_service_uuid = ParcelUuid.fromString("03b80e5a-ede8-4b33-a751-6ce34ec4c700");
    static ParcelUuid freedrum_service__uuid = ParcelUuid.fromString("0e5a1523-ede8-4b33-a751-6ce34ec47c00");
    final static ParcelUuid freedrum_char_orientation = ParcelUuid.fromString("0e5a1525-ede8-4b33-a751-6ce34ec47c00");
    final static ParcelUuid freedrum_drum_conf = ParcelUuid.fromString("0e5a1526-ede8-4b33-a751-6ce34ec47c00");
    final static ParcelUuid freedrum_global_conf = ParcelUuid.fromString("0e5a1527-ede8-4b33-a751-6ce34ec47c00");
    final static ParcelUuid freedrum_status_uuid = ParcelUuid.fromString("0e5a1528-ede8-4b33-a751-6ce34ec47c00");

    final static ParcelUuid apple_midi_io_char=ParcelUuid.fromString("7772e5db-3868-4112-a1a9-f2669d106bf3");
    //    final UUID apple_midi_service=UUID.fromString("03b80e5a-ede8-4b33-a751-6ce34ec4c700");


    @Override
    protected void onPause() {
        super.onPause();
        scanLeDevice(false);
    }

    List<BluetoothDevice> deviceList = new ArrayList<>();


    ArrayAdapter<Drumstick> mLeDeviceListAdapter;

    View deviceView;
    @Override
    public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
        Drumstick device = mLeDeviceListAdapter.getItem(position);

        deviceView=view;
        Intent intent=new Intent(this, StickViewActivity.class);
        intent.putExtra("address",device.getAddress());
        startActivity(intent);


    }
}
/*
    private void displayGattServices(List<BluetoothGattService> gattServices) {

        UUID freedrum_service_uuid=UUID.fromString("asdf");
        if (gattServices == null) return;
        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {

            if (freedrum_service_uuid.equals(gattService.getUuid())){
                List<BluetoothGattCharacteristic> gattCharacteristics =
                        gattService.getCharacteristics();

                gattService.getCharacteristic()
                for (BluetoothGattCharacteristic gattCharacteristic :
                        gattCharacteristics) {
                    charas.add(gattCharacteristic);
                    HashMap<String, String> currentCharaData =
                            new HashMap<String, String>();
                    uuid = gattCharacteristic.getUuid().toString();
                    currentCharaData.put(
                            LIST_NAME, SampleGattAttributes.lookup(uuid,
                                    unknownCharaString));
                    currentCharaData.put(LIST_UUID, uuid);
                    gattCharacteristicGroupData.add(currentCharaData);
                }

            }
            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.
                            lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();

            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();
            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic :
                    gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData =
                        new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid,
                                unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }
}
*/
