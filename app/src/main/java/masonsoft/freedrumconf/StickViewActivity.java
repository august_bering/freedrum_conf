package masonsoft.freedrumconf;

import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.content.*;
import android.content.pm.ActivityInfo;
import android.databinding.DataBindingUtil;
import android.graphics.PointF;
import android.media.MediaPlayer;
import android.media.midi.*;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import freedrum.freedrumsdk.Drumstick;
import freedrum.freedrumsdk.FreedrumController;
import masonsoft.freedrumconf.databinding.FragmentSendDrumConfBinding;
import freedrum.freedrumsdk.proto.Config;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import javax.microedition.khronos.opengles.GL10;

public class StickViewActivity extends AppCompatActivity {

	private static final String TAG = "freedrum";
	private static final int NR_PADS = 6;
	private StickView stickView;
//	private BluetoothAdapter mBluetoothAdapter;
//	private BluetoothDevice bluetoothDevice;
//	static BluetoothGatt bluetoothGatt;
	private TextView statusview;
	private LocalBroadcastManager localBroadcastManager;
	private BluetoothGattCharacteristic midiCharacteristic;
	private MediaPlayer soundSnare;
	private MidiDevice freedrumMidiDevice;
//	public static BluetoothGattCharacteristic c2;
	private BluetoothGattCharacteristic orientationCharacteristic;
	private FreedrumController freedrumController;
	private Drumstick drumstick;

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()){
			case android.R.id.edit:
				Intent intent = new Intent(this, StickConfigActivity.class);
				intent.putExtra("address",drumstick.getAddress());
				startActivity(intent);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		freedrumController=FreedrumController.instance(this);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		setContentView(R.layout.activity_stick_view);


		final BluetoothManager bluetoothManager =
				(BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
//		mBluetoothAdapter = bluetoothManager.getAdapter();
		ViewGroup content = (ViewGroup) findViewById(R.id.surfaceContainer);
		/*MyGLSurfaceView mGLView = new MyGLSurfaceView(this);

        content.addView(mGLView);*/
		stickView = (StickView) findViewById(R.id.stickview);
		//content.addView(stickView);

		localBroadcastManager = LocalBroadcastManager.getInstance(this);

		String address = getIntent().getStringExtra("address");

//		bluetoothDevice = mBluetoothAdapter.getRemoteDevice(address);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

//		bluetoothDevice.createBond();

		statusview = (TextView) findViewById(R.id.status);
		stickView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				final FragmentSendDrumConfBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_send_drum_conf, null, false);
				String format = String.format("%.2f,\t%.2f", stickView.lastHit.x, stickView.lastHit.y);
				final PointF pos = new PointF(stickView.lastHit.x, stickView.lastHit.y);
				AlertDialog alertDialog = new AlertDialog.Builder(StickViewActivity.this).setView(binding.getRoot()).setPositiveButton("Send", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						sendPadConf(Integer.decode(binding.padNr.getText().toString()), Integer.decode(binding.midiNote.getText().toString()), pos);
					}
				}).setTitle("Set pad config").create();

				binding.midiNote.setText("45");
				binding.padNr.setText("0");


				binding.positions.setText(format);
				alertDialog.show();

			}
		});
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
		View decorView = getWindow().getDecorView();
// Hide both the navigation bar and the status bar.
// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
// a general rule, you should design your app to hide the status bar whenever you
// hide the navigation bar.
		int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_FULLSCREEN |
				View.SYSTEM_UI_FLAG_IMMERSIVE;
		//decorView.setSystemUiVisibility(uiOptions);
//		getdec.setSystemUiVisibility(
//				View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//						| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//						| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//						| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
//						| View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
//						| View.SYSTEM_UI_FLAG_IMMERSIVE);



		/*if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
			MidiDeviceInfo[] infos = m.getDevices();
			m.openDevice(infos[0], new MidiManager.OnDeviceOpenedListener() {
				@Override
				public void onDeviceOpened(MidiDevice device) {
					MidiOutputPort midiOutputPort = device.openOutputPort(0);
					midiOutputPort.connect(new MidiReceiver() {
						@Override
						public void onSend(byte[] msg, int offset, int count, long timestamp) throws IOException {

							int i=msg.length;
						}
					});
				}
			},null);
		}*/

		soundSnare = MediaPlayer.create(this, R.raw.snare);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.stickview,menu	);
		return true;
	}

	private void sendPadConf(int padNr, int midiNote, PointF pos) {

		drumstick.sendPadConf(padNr,midiNote,pos.y,pos.x);
	}

	@Override
	protected void onPause() {
		super.onPause();
		drumstick.disconnect();
		localBroadcastManager.unregisterReceiver(broadcastReceiver);
//		MyService.diconnect(this);
	}

	@Override
	protected void onStart() {
		super.onStart();
		if (connectMidi) {
			MidiManager m = (MidiManager) this.getSystemService(Context.MIDI_SERVICE);
			m.openBluetoothDevice(MyService.instance.bluetoothDevice, new MidiManager.OnDeviceOpenedListener() {
				@Override
				public void onDeviceOpened(MidiDevice device) {
					freedrumMidiDevice = device;
					MidiOutputPort midiOutputPort = device.openOutputPort(0);

					midiOutputPort.connect(new MidiReceiver() {
						@Override
						public void onSend(byte[] msg, int offset, int count, long timestamp) throws IOException {
							int pos = offset;
							int command = 0xf & (msg[pos] >> 4);
							int channel = 0xf & (msg[pos]);
							pos++;
							switch (command) {
								case 0x9: {
									int note = msg[pos++] & 0xff;
									int velocity = msg[pos++] & 0xff;
									stickView.reportHit(note, velocity);
									if (soundSnare.isPlaying())
										soundSnare.stop();
									soundSnare.start();

								}
								break;
							}

						}
					});

				}
			}, null);
		}
	}

	@Override
	public void onBackPressed() {
		drumstick.disconnect();
		super.onBackPressed();
	}

	boolean connectMidi = false;

	@Override
	protected void onStop() {
		super.onStop();
		if (freedrumMidiDevice != null) {
			try {
				freedrumMidiDevice.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		String address = getIntent().getStringExtra("address");
		statusview.setText("Connecting...");
		//MyService.connect(this,address);
		final Handler handler = new Handler();
		drumstick = freedrumController.getDrumstick(address,new DrumstickCallback(handler));
		drumstick.connect();


		localBroadcastManager.registerReceiver(broadcastReceiver, new IntentFilter("orientation"));
	}

	public static Config.ConfigSet drumConfig = Config.ConfigSet.newBuilder().build();
/*
	public void readContigs() {
		byte[] val = {Config.Command.ReadSingleConf_VALUE};
		c2.setValue(val);
		bluetoothGatt.writeCharacteristic(c2);
	}

	public void saveContig() {
		byte[] val = {Config.Command.SaveConfig_VALUE};
		c2.setValue(val);
		bluetoothGatt.writeCharacteristic(c2);
	}*/



	static long lastHit = 0;
	BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			byte[] value = intent.getByteArrayExtra("data");
			ByteBuffer wrap = ByteBuffer.wrap(value);
			wrap.order(ByteOrder.LITTLE_ENDIAN);
			byte type = wrap.get();
			//wrap.position(4);
			float x = wrap.getFloat();
			float y = wrap.getFloat();
			float z = wrap.getFloat();

			if (type == 2) {
				//hit
				int note = wrap.get() & 0xff;
				int strength = wrap.get() & 0xff;
//				stickView.setStickHitPos(z, x);
				long millis = System.currentTimeMillis();
				if (lastHit > millis - 100)
					;
				else {
					stickView.reportHit(note, strength);
					playSound(strength);
					Log.d(TAG, String.format("note:%d, velo: %d", note, strength));
					lastHit = millis;

				}
			}
			stickView.setStickPos(z, x);
			String format = String.format("%.2f,\t%.2f,\t%.2f", x, y, z);
			if (statusview != null) {
				statusview.setText(format);
			}
			//Log.d(TAG,format);

		}
	};

	private void playSound(int strength) {


		soundSnare.stop();
		soundSnare.release();
		soundSnare = MediaPlayer.create(this, R.raw.snare);
		float vol = (float) ((float) (strength + 1) * Math.E / 127);
		soundSnare.setVolume(vol, vol);
		soundSnare.start();
	}

	class MyGLSurfaceView extends GLSurfaceView {

		private final MyGLRenderer mRenderer;

		public MyGLSurfaceView(Context context) {
			super(context);

			// Create an OpenGL ES 2.0 context
			setEGLContextClientVersion(2);
			mRenderer = new MyGLRenderer();

			// Set the Renderer for drawing on the GLSurfaceView
			setRenderer(mRenderer);
		}
	}

	public class MyGLRenderer implements GLSurfaceView.Renderer {


		public void onDrawFrame(GL10 unused) {
			// Redraw background color
			GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
		}

		@Override
		public void onSurfaceCreated(GL10 gl, javax.microedition.khronos.egl.EGLConfig config) {
			GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		}

		public void onSurfaceChanged(GL10 unused, int width, int height) {
			GLES20.glViewport(0, 0, width, height);
		}
	}

	private class DrumstickCallback implements freedrum.freedrumsdk.DrumstickCallback {
		private final Handler handler;

		public DrumstickCallback(Handler handler) {
			this.handler = handler;
		}

		@Override
		public void connected() {
			handler.post(new Runnable() {
				@Override
				public void run() {
					statusview.setText("Connected");
					drumstick.subscribeOrientation(true);
					drumstick.subscribeStatus(true);
				}
			});
		}

		@Override
		public void disconnected() {

		}

		@Override
		public void midiIn(int note, boolean on, int velocity) {

			long millis = System.currentTimeMillis();
			if (lastHit > millis - 100)
				;
			else {
				stickView.reportHit(note, velocity);
				playSound(velocity);
				Log.d(TAG, String.format("note:%d, velo: %d", note, velocity));
				lastHit = millis;

			}

		}

		@Override
		public void orientationUpdate(float x, float y, float z) {
			stickView.setStickPos(z, y);
//			String format = String.format("%.2f,\t%.2f,\t%.2f", x, y, z);
//			if (statusview != null) {
//				statusview.setText(format);
//			}
		}

		@Override
		public void configRead(Config.SingleDrumConf conf) {

		}

		@Override
		public void globalConfRead(Config.GlobalConf globalConf) {

		}

		@Override
		public void statusUpdate(final Drumstick.Status currentStatus) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					String format = String.format("compass:%s, still:%s", currentStatus.compassValid, currentStatus.isStill);
					statusview.setText(format);
				}
			});
		}
	}
}
