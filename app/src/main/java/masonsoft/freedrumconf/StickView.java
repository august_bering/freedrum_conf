package masonsoft.freedrumconf;

import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by august on 8/14/16.
 */
public class StickView extends SurfaceView {
	private Paint outofboundsPaint;
//	private float maxAngle= (float) (Math.PI/2);
	final float Z_MAX= (float) Math.toRadians(90);
	final float Y_MAX= (float) Math.toRadians(70);
	private RectF angularSpace;
	private Bitmap background;
	private Rect screenRect;
	private float lastZ;
	private float lastY;
	private int lastNote;
	private int lastVelocity;

	public StickView(Context context) {
		super(context);

		init();
//		context.getDrawable(R.drawable.)
	}

	private void init() {
		angularSpace=new RectF(-Z_MAX,-Y_MAX,Z_MAX,Y_MAX);
		backroundPaint.setColor(Color.WHITE);
		linePaint=new Paint();
		linePaint.setColor(Color.BLUE);
		outofboundsPaint=new Paint();
		outofboundsPaint.setColor(Color.RED);
		background = BitmapFactory.decodeResource(getContext().getResources(),
				R.drawable.background_pads);

		lastHitPaint.setColor(Color.WHITE);
	}

	public StickView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	Paint backroundPaint=new Paint(),linePaint;
	Paint lastHitPaint=new Paint();
	public void setStickHitPos(float z, float y) {
		lastHit.set(z,y);
	}

	public void setStickPos(float z, float y) {
		SurfaceHolder holder = getHolder();

		Canvas canvas = holder.lockCanvas();
		if (canvas==null)
			return;
		canvas.drawBitmap(background,null,screenRect,null);
		//canvas.drawPaint(backroundPaint);

		y=-y;
		lastZ=z;
		lastY=y;
		Paint paint=linePaint;
		canvas.setMatrix(matrix);

		if (!angularSpace.contains(z,y)) {
			paint = outofboundsPaint;
			z=limit(z,angularSpace.left,angularSpace.right);
			y=limit(angularSpace.top,y,angularSpace.bottom);
		}
//		if (z>maxAngle) {
//			z = maxAngle;
//			paint=outofboundsPaint;
//		} else if (z<) {
//			z = 0;
//			paint=outofboundsPaint;
//		}
//		if (y>maxAngle) {
//			y = maxAngle;
//			paint=outofboundsPaint;
//		} else if (y<0) {
//			y = 0;
//			paint=outofboundsPaint;
//		}

		lastHitPaint.setAlpha(lastVelocity*2+20);

		canvas.drawCircle(lastHit.x,lastHit.y,0.05f,lastHitPaint);


		canvas.drawCircle(z,y,0.05f,paint);
		holder.unlockCanvasAndPost(canvas);
	}

	PointF lastHit=new PointF();

	private float limit(float z, float min, float max) {
		return z<min ? min : (z>max?max:z);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		matrix.setRectToRect(angularSpace,new RectF(0,0,w,h), Matrix.ScaleToFit.FILL);//Scale(w/maxAngle,h/maxAngle);
		screenRect=new Rect(0,0,w,h);
	}
	Matrix matrix=new Matrix();

	public void reportHit(int note, int velocity) {
		lastNote=note;
		lastVelocity=velocity;
		lastHit.set(lastZ,lastY);
	}
}
