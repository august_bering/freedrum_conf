package masonsoft.freedrumconf;

import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.List;


public class MyService extends IntentService {
	private static final String TAG = "freedrum";
	public static final String CONNECT = "connect";
	private BluetoothGattCharacteristic orientationCharacteristic;
	private BluetoothGattCharacteristic midiCharacteristic;
	private BluetoothAdapter mBluetoothAdapter;
	BluetoothDevice bluetoothDevice;
	BluetoothGatt bluetoothGatt;
	public static MyService instance;
	private BluetoothGattCharacteristic c2;
	private BluetoothGattCharacteristic globalConfigChar;


	public MyService() {
		super("freedrum com");
	}

	@Override
	public void onCreate() {
		super.onCreate();
//		localBroadcastManager.registerReceiver(new BroadcastReceiver() {
//			@Override
//			public void onReceive(Context context, Intent intent) {
//
//			}
//		}, new IntentFilter("sendBLE"));

		final BluetoothManager bluetoothManager =
				(BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = bluetoothManager.getAdapter();

		instance=this;
	}

	LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);

	@Override
	protected void onHandleIntent(Intent intent) {


		switch (intent.getAction()) {
			case CONNECT: {
				if (bluetoothDevice==null) {
					String address = intent.getStringExtra("address");

					bluetoothDevice = mBluetoothAdapter.getRemoteDevice(address);

					bluetoothDevice.createBond();
					start();
				}
				break;
			}
			case "disconnect": {
				bluetoothGatt.disconnect();
				break;
			}
			case "sendConf": {
				byte[] data = intent.getByteArrayExtra("data");
				c2.setValue(data);
				bluetoothGatt.writeCharacteristic(c2);
			}
			break;
		}
	}


	public void start() {


		bluetoothGatt = bluetoothDevice.connectGatt(this, true, new BluetoothGattCallback() {
			@Override
			public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
				super.onCharacteristicWrite(gatt, characteristic, status);
			}

			@Override
			public void onConnectionStateChange(BluetoothGatt gatt, int status,
												int newState) {
				String intentAction;
				if (newState == BluetoothProfile.STATE_CONNECTED) {
					String msg = "Connected to GATT server.";
					logMsg(msg);
					logMsg("Attempting to start service discovery:" +
							gatt.discoverServices());

				} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {

					logMsg("Disconnected from GATT server.");
//                    statusview.setText( "Disconnected from GATT server.");
				}
			}

			@Override
			public void onServicesDiscovered(BluetoothGatt gatt, int status) {
				switch (status) {
					case BluetoothGatt.GATT_SUCCESS: {
						BluetoothGattService service = gatt.getService(BLEScanActivitiy.freedrum_service__uuid.getUuid());
						orientationCharacteristic = service.getCharacteristic(BLEScanActivitiy.freedrum_char_orientation.getUuid());
						gatt.setCharacteristicNotification(orientationCharacteristic, true);

						List<BluetoothGattDescriptor> descriptors = orientationCharacteristic.getDescriptors();
						BluetoothGattDescriptor descriptor = descriptors.get(0);
						descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
						gatt.beginReliableWrite();
						gatt.writeDescriptor(descriptor);


						c2 = service.getCharacteristic(BLEScanActivitiy.freedrum_drum_conf.getUuid());
						gatt.setCharacteristicNotification(c2, true);

						descriptor=c2.getDescriptors().get(0);
						descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
						gatt.writeDescriptor(descriptor);
						gatt.executeReliableWrite();

						globalConfigChar = service.getCharacteristic(BLEScanActivitiy.freedrum_global_conf.getUuid());

					}
					/*
					{
						BluetoothGattService midiservice = gatt.getService(BLEScanActivitiy.apple_midi_service_uuid.getUuid());
						midiCharacteristic = midiservice.getCharacteristic(BLEScanActivitiy.apple_midi_io_char.getUuid());
						gatt.setCharacteristicNotification(midiCharacteristic, true);

						List<BluetoothGattDescriptor> descriptors = midiCharacteristic.getDescriptors();
						BluetoothGattDescriptor descriptor = descriptors.get(0);
						descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
						gatt.writeDescriptor(descriptor);

					}*/
					break;
				}
			}

			void subscribeOrientation(boolean enable){
				List<BluetoothGattDescriptor> descriptors = orientationCharacteristic.getDescriptors();
				BluetoothGattDescriptor descriptor = descriptors.get(0);

				descriptor.setValue(enable ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);

			}

			long lastMessage;

			@Override
			public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
				if (characteristic == midiCharacteristic) {
					byte[] value = characteristic.getValue();
					//play note?
				} else if (characteristic == orientationCharacteristic) {
					long timeMillis = System.currentTimeMillis();
					byte[] value = characteristic.getValue();
					if (value[0] == 2 || timeMillis > lastMessage + 40) {

						Intent intent = new Intent("orientation");
						intent.putExtra("data", value);

						localBroadcastManager.sendBroadcast(intent);
						lastMessage = timeMillis;
					}
				} else if (characteristic == c2) {
					bluetoothGatt.readCharacteristic(c2);

					byte[] value = c2.getValue();
					Intent intent = new Intent("readConfig");
					intent.putExtra("data", value);
					localBroadcastManager.sendBroadcast(intent);

				}
			}
		});

	}

	private int logMsg(String msg) {
		Intent intent=new Intent("bleLog");
		intent.putExtra("message",msg);
		localBroadcastManager.sendBroadcast(intent);
		return Log.i(TAG, msg);
	}


	public static void connect(Context context, String address) {
		Intent intent = new Intent(context, MyService.class);
		intent.setAction(CONNECT);
		intent.putExtra("address", address);
		context.startService(intent);
	}

	public static void diconnect(Context context) {
//		Intent intent = new Intent(context, MyService.class);
//		intent.setAction("disconnect");
		instance.bluetoothGatt.disconnect();
	}


	public void sendConfData(byte[] bytes) {
		if (c2!=null) {
			c2.setValue(bytes);
			bluetoothGatt.writeCharacteristic(c2);
		}
	}

}
