package masonsoft.freedrumconf;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import freedrum.freedrumsdk.Drumstick;
import freedrum.freedrumsdk.DrumstickCallback;
import freedrum.freedrumsdk.FreedrumController;
import masonsoft.freedrumconf.databinding.ActivityStickConfigBinding;
import masonsoft.freedrumconf.databinding.FragmentSendDrumConfBinding;
import freedrum.freedrumsdk.proto.Config;

public class StickConfigActivity extends AppCompatActivity {

	private static final int NR_PADS = 6;
	private ActivityStickConfigBinding binding;
	private Config.ConfigSet configs;
	private BluetoothGattCharacteristic confChar;
	private Drumstick drumstick;
	private DrumstickCallback drumstickCallback=new DrumstickCallback() {
		@Override
		public void connected() {

		}

		@Override
		public void disconnected() {

		}

		@Override
		public void midiIn(int note, boolean on, int velocity) {

		}

		@Override
		public void orientationUpdate(float x, float y, float z) {

		}

		@Override
		public void configRead(Config.SingleDrumConf singleDrumConf) {
			int padNr = singleDrumConf.getPadNr();
			configs.getConfigsList().remove(padNr);
			configs.getConfigsList().add(padNr, singleDrumConf.getConf());
		}

		@Override
		public void globalConfRead(final Config.GlobalConf globalConf) {
			mHandler.post(new Runnable() {
				@Override
				public void run() {
					binding.threshold.setText(Integer.toString(globalConf.getHitThreshold()));
					binding.sensitivity.setText(Integer.toString(globalConf.getHitSensitivity()));
					binding.scantime.setText(Integer.toString(globalConf.getScanTime()));
					binding.masktime.setText(Integer.toString(globalConf.getTriggerMaskTime()));

				}
			});

		}

		@Override
		public void statusUpdate(Drumstick.Status currentStatus) {

		}
	};
	private Handler mHandler;

	class PadConfViewHolder extends RecyclerView.ViewHolder {

		FragmentSendDrumConfBinding confBinding;
		private Config.DrumConfig conf;

		public PadConfViewHolder(FragmentSendDrumConfBinding binding) {
			super(null);
			confBinding=binding;
		}

		public void setConf(Config.DrumConfig conf) {
			this.conf = conf;
			confBinding.midiNote.setText(conf.getMidiNote());

			String format = String.format("%.2f,\t%.2f", conf.getZpos(), conf.getYpos());

			confBinding.positions.setText(format);
		}

		public Config.DrumConfig getConf() {
			return conf;
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		String address = getIntent().getStringExtra("address");
		FreedrumController freedrumController = FreedrumController.instance(this);
		drumstick = freedrumController.getDrumstick(address,drumstickCallback);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mHandler = new Handler();
		configs = StickViewActivity.drumConfig;

		LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				byte[] value = intent.getByteArrayExtra("data");
				ByteArrayInputStream inp = new ByteArrayInputStream(value, 1, value.length - 1);

				switch (value[0]) {
					case Config.Command.WriteSingleConf_VALUE:
						try {
							Config.SingleDrumConf singleDrumConf = Config.SingleDrumConf.parseFrom(inp);
							int padNr = singleDrumConf.getPadNr();
							configs.getConfigsList().remove(padNr);
							configs.getConfigsList().add(padNr, singleDrumConf.getConf());

						} catch (IOException e) {
							e.printStackTrace();
						}
						break;
					case Config.Command.ConfHitThreshold_VALUE:
						binding.threshold.setText(Integer.toString(value[1]));
						break;
				}
			}
		}, new IntentFilter("readConfig"));

		binding = DataBindingUtil.setContentView(this, R.layout.activity_stick_config);
		binding.padList.setAdapter(new RecyclerView.Adapter<PadConfViewHolder>() {
			@Override
			public PadConfViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
				FragmentSendDrumConfBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.fragment_send_drum_conf, parent, false);
				return new PadConfViewHolder(binding);
			}

			@Override
			public void onBindViewHolder(PadConfViewHolder holder, int position) {

				Config.DrumConfig drumConfig = StickConfigActivity.this.configs.getConfigs(position);
				holder.setConf(drumConfig);

			}

			@Override
			public int getItemCount() {
				return configs.getConfigsCount();
			}
		});

	}

	public void disableAllPads() {
		try {
			MyService myService = MyService.instance;
			BluetoothGatt bluetoothGatt = myService.bluetoothGatt;
			bluetoothGatt.beginReliableWrite();

			for (int i = 0; i < NR_PADS; i++) {
				Config.DrumConfig drumConfig = Config.DrumConfig.newBuilder().setMidiNote(0).build();
				Config.SingleDrumConf singleDrumConf = Config.SingleDrumConf.newBuilder().setPadNr(i).setConf(drumConfig).build();
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream(100);
				outputStream.write(Config.Command.WriteSingleConf_VALUE);
				singleDrumConf.writeTo(outputStream);
				myService.sendConfData(outputStream.toByteArray());
			}
			bluetoothGatt.executeReliableWrite();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.stick_config_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.readConfigs:
				readContigs();
				break;
			case R.id.disableAll:
				disableAllPads();
				break;
			case R.id.actionSave:
				ByteArrayOutputStream outputStream = new ByteArrayOutputStream(10);
				outputStream.write(Config.Command.SaveConfig_VALUE);
				MyService.instance.sendConfData(outputStream.toByteArray());
				break;
			case R.id.renameDevice:
			{
//				new AlertDialog.Builder(this).
			}
		}
		return super.onOptionsItemSelected(item);
	}

	public void readContigs() {

		drumstick.readContigs();
	}
}
