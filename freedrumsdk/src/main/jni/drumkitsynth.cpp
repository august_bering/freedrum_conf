#include <jni.h>
#include <malloc.h>
#include <SLES/OpenSLES_AndroidConfiguration.h>
#include <SLES/OpenSLES.h>
#include <AndroidIO/SuperpoweredAndroidAudioIO.h>
#include <SuperpoweredSimple.h>
#include "fdsynth/DrumkitSynth.h"
#include <android/log.h>

static DrumkitSynth drumkit;
static float volA=1;

extern "C" {
SuperpoweredAndroidAudioIO *audioSystem;
float *stereoBuffer;
static bool audioProcessing(void *clientdata, short int *audioIO, int numberOfSamples, int __unused samplerate) {
    bool silence = !drumkit.process(stereoBuffer,numberOfSamples,volA);
    // The stereoBuffer is ready now, let's put the finished audio into the requested buffers.
    if (!silence) SuperpoweredFloatToShortInt(stereoBuffer, audioIO, numberOfSamples);

    return !silence;

}

JNIEXPORT void JNICALL
Java_freedrum_freedrumsdk_DrumkitSynth_initAudio(JNIEnv *env, jobject instance, jint samplerate,
                                                 jint buffersize) {

    stereoBuffer = (float *)memalign(16, (buffersize + 16) * sizeof(float) * 2);
    audioSystem = new SuperpoweredAndroidAudioIO(samplerate, buffersize, false, true, audioProcessing,
                                                 nullptr, -1, SL_ANDROID_STREAM_MEDIA, buffersize * 2);


}

JNIEXPORT void JNICALL
Java_freedrum_freedrumsdk_DrumkitSynth_playNote(JNIEnv *env, jobject instance, jint note,
                                                    jint velocity) {

    drumkit.playNote(note, velocity);

}

JNIEXPORT jboolean JNICALL
Java_freedrum_freedrumsdk_DrumkitSynth_loadDrumkit(JNIEnv *env, jobject instance,
                                                       jstring drumdir) {
    const char *path = env->GetStringUTFChars(drumdir, 0);
    __android_log_print(ANDROID_LOG_INFO, "Freedrumsynth", "Loading kit: %s",path);

    drumkit.loadKit(path)

    env->ReleaseStringUTFChars(drumdir, path);


    return true;
}
}