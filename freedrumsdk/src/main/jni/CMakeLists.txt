cmake_minimum_required(VERSION 3.4.1)

set(
	PATH_TO_SUPERPOWERED
	CACHE STRING ""
)

message(${ANDROID_ABI})

file(GLOB CPP_FILES "*.cpp")
file(GLOB_RECURSE SF2_FILES "fdsynth/*.cpp")

add_library(
	FreedrumSynth
	SHARED
	${CPP_FILES}
	${SF2_FILES}
	${PATH_TO_SUPERPOWERED}/AndroidIO/SuperpoweredAndroidAudioIO.cpp
)

include_directories(src/main/jni)
include_directories(src/main/jni/fdsynth)
include_directories(${PATH_TO_SUPERPOWERED})

target_link_libraries(
    FreedrumSynth
    log
    android
    OpenSLES
    ${PATH_TO_SUPERPOWERED}/libSuperpoweredAndroid${ANDROID_ABI}.a
)