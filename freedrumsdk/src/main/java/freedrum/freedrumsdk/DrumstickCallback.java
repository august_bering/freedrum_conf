package freedrum.freedrumsdk;

import freedrum.freedrumsdk.proto.Config;

/**
 * Created by fredrik on 2017-06-07.
 */
public interface DrumstickCallback {
    void connected();

    void disconnected();

    void midiIn(int note, boolean on, int velocity);

    void orientationUpdate(float x, float y, float z);

    void configRead(Config.SingleDrumConf conf);

    void globalConfRead(Config.GlobalConf globalConf);

    void statusUpdate(Drumstick.Status currentStatus);
}
