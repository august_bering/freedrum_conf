package freedrum.freedrumsdk;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import freedrum.freedrumsdk.proto.Config;

import static android.bluetooth.BluetoothProfile.STATE_CONNECTED;

/**
 * Created by august on 4/7/17.
 */

public class Drumstick {
	private BluetoothDevice device;
	private Context context;
	private BluetoothGatt bluetoothGatt;
	private BluetoothGattCharacteristic orientationCharacteristic;
	private BluetoothGattCharacteristic configCharacteristic;
	private BluetoothGattCharacteristic globalConfigChar;
	private BluetoothGattCharacteristic midiCharacteristic;
	public long rssiTime;
	public int rssiVal;
	private BluetoothGattCharacteristic statusCharacteristic;
	private String LOG_TAG="drumstick";

	public String getAddress() {
		return device.getAddress();
	}

	@Override
	public String toString() {
		return device.getAddress();
	}

	DrumstickCallback callback;

	public Drumstick(BluetoothDevice device, Context context) {

		this.device = device;
		this.context = context;
		context.registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String device = intent.getStringExtra(BluetoothDevice.EXTRA_DEVICE);
				int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE,-1);
				if (BluetoothDevice.BOND_BONDED==state){
					connect();
				}
			}
		}, new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED));
	}

	public void subscribeOrientation(boolean enable) {
		log("subs "+enable);
		subscribeChar(orientationCharacteristic,enable);
	}
	public boolean subscribeStatus(boolean enable){
		if (statusCharacteristic!=null) {
			subscribeChar(statusCharacteristic,enable);
			return true;
		}
		return false;
	}

	private void subscribeChar(BluetoothGattCharacteristic ch, boolean enable) {
		if (ch==null){
			Log.w(LOG_TAG,"Null char cant be subscribed");
			return;
		}
		bluetoothGatt.setCharacteristicNotification(ch, enable);
		List<BluetoothGattDescriptor> descriptors = ch.getDescriptors();
		if (descriptors.isEmpty()){
			Log.e(LOG_TAG, "Can't subscribe to characteric, no descriptors."+ch.toString());
			return;
		}
		BluetoothGattDescriptor descriptor = descriptors.get(0);
		descriptor.setValue(enable?BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE:BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
		bluetoothGatt.writeDescriptor(descriptor);
	}

	public boolean isConnected() {
		final BluetoothManager bluetoothManager =
				(BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);

		return bluetoothGatt!=null && STATE_CONNECTED==bluetoothManager.getConnectionState(device,BluetoothProfile.GATT_SERVER);
	}

	public void connect() {

		if (isConnected())
			return;
		if (device.getBondState()==BluetoothDevice.BOND_NONE) {
			device.createBond();
			return;//we'll get called again when bonding completes
		}


		bluetoothGatt = device.connectGatt(context, true, new BluetoothGattCallback() {
			@Override
			public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
				super.onCharacteristicWrite(gatt, characteristic, status);

				Runnable cmd = commandCue.poll();
				if (cmd != null)
					cmd.run();
			}

			@Override
			public void onConnectionStateChange(BluetoothGatt gatt, int status,
												int newState) {
				String intentAction;
				if (newState == STATE_CONNECTED) {
					gatt.discoverServices();

				} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
					callback.disconnected();
				}
			}

			@Override
			public void onServicesDiscovered(BluetoothGatt gatt, int status) {
				switch (status) {
					case BluetoothGatt.GATT_SUCCESS: {
						BluetoothGattService service = gatt.getService(FreedrumController.freedrum_service__uuid.getUuid());
						orientationCharacteristic = service.getCharacteristic(FreedrumController.freedrum_char_orientation.getUuid());

						gatt.beginReliableWrite();


						configCharacteristic = service.getCharacteristic(FreedrumController.freedrum_drum_conf.getUuid());
						subscribeChar(configCharacteristic,true);
						gatt.executeReliableWrite();

						globalConfigChar = service.getCharacteristic(FreedrumController.freedrum_global_conf.getUuid());

						statusCharacteristic = service.getCharacteristic(FreedrumController.freedrum_status_uuid.getUuid());
						
						gatt.executeReliableWrite();
					}

					{
						BluetoothGattService midiservice = gatt.getService(FreedrumController.apple_midi_service_uuid.getUuid());
						midiCharacteristic = midiservice.getCharacteristic(FreedrumController.apple_midi_io_char.getUuid());
						gatt.setCharacteristicNotification(midiCharacteristic, true);

						/*List<BluetoothGattDescriptor> descriptors = midiCharacteristic.getDescriptors();
						BluetoothGattDescriptor descriptor = descriptors.get(0);
						descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
						gatt.writeDescriptor(descriptor);*/

					}

					callback.connected();

					break;
				}
			}


			@Override
			public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
				if (characteristic == midiCharacteristic) {
					byte[] value = characteristic.getValue();
					//play note?
				} else if (characteristic == orientationCharacteristic) {
					long timeMillis = System.currentTimeMillis();
					byte[] value = characteristic.getValue();

					ByteBuffer wrap = ByteBuffer.wrap(value);
					wrap.order(ByteOrder.LITTLE_ENDIAN);
					byte type = wrap.get();
					//wrap.position(4);
					float x = wrap.getFloat();
					float y = wrap.getFloat();
					float z = wrap.getFloat();
					callback.orientationUpdate(x, y, z);

					if (type == 2) {
						//hit
						int note = wrap.get() & 0xff;
						int strength = wrap.get() & 0xff;
						callback.midiIn(note, true, strength);
					}

				} else if (characteristic == configCharacteristic) {

					byte[] value = configCharacteristic.getValue();

					parseIncoming(value);

				}
				else if (characteristic==statusCharacteristic){

					String s = statusCharacteristic.getStringValue(0);
					Matcher matcher = statusPattern.matcher(s);
					if (matcher.matches()) {
						currentStatus.compassValid = Integer.parseInt(matcher.group(1))!=0;
						currentStatus.isStill = Integer.parseInt(matcher.group(2))!=0;

						callback.statusUpdate(currentStatus);
					}
				}
			}
		});

	}
	static Pattern statusPattern=Pattern.compile("m(.)s(.)");

	public class Status{
		public boolean compassValid;
		public boolean isStill;
	}

	Status currentStatus=new Status();
	public void disconnect() {
		bluetoothGatt.disconnect();
		bluetoothGatt.close();
	}



	public void readContigs() {
//        bluetoothGatt.beginReliableWrite();

		for (int i = 0; i < 6; i++) {
			byte[] val = {Config.Command.ReadSingleConf_VALUE, (byte) i};
			sendConfData(val);
		}
		byte[] val = {Config.Command.ReadGlobalConf_VALUE};
		sendConfData(val);
		//     bluetoothGatt.executeReliableWrite();
	}

	public void setScantime(int scantime) {
		globalConf.setScanTime(scantime);
	}

	public void setHitSensitivity(int sensitivity) {
		globalConf.setHitSensitivity(sensitivity);
	}

	public Config.GlobalConf.Builder getGlobalConf() {
		return globalConf;
	}

	public void sendGlobalConf() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(100);
		outputStream.write(Config.Command.WriteGlobalConf_VALUE);
		try {
			globalConf.build().writeTo(outputStream);
			sendConfData(outputStream.toByteArray());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendPadConf(int padNr, int midiNote, float z, float y) {
		Config.DrumConfig drumConfig = Config.DrumConfig.newBuilder().setYpos(y).setZpos(z).setMidiNote(midiNote).build();
		Config.SingleDrumConf singleDrumConf = Config.SingleDrumConf.newBuilder().setPadNr(padNr).setConf(drumConfig).build();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(100);
		outputStream.write(Config.Command.WriteSingleConf_VALUE);
		try {
			singleDrumConf.writeTo(outputStream);
			sendConfData(outputStream.toByteArray());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	Queue<Runnable> commandCue = new ArrayDeque<>();

	private void sendConfData(final byte[] bytes) {
		if (configCharacteristic != null) {
			configCharacteristic.setValue(bytes);
			boolean sent = bluetoothGatt.writeCharacteristic(configCharacteristic);
			log("sent conf char res:" + sent);
			if (!sent) {
				commandCue.add(new Runnable() {
					@Override
					public void run() {
						configCharacteristic.setValue(bytes);
						boolean sent = bluetoothGatt.writeCharacteristic(configCharacteristic);
					}
				});
			}
		}
	}

	private void log(String s) {

		Log.d(LOG_TAG ,s);
	}

	public void commitConfig() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(10);
		outputStream.write(Config.Command.SaveConfig_VALUE);
		sendConfData(outputStream.toByteArray());
	}

	void parseIncoming(byte[] value) {
		ByteArrayInputStream inp = new ByteArrayInputStream(value, 1, value.length - 1);
		try {

			switch (value[0]) {
				case Config.Command.WriteSingleConf_VALUE:
					Config.SingleDrumConf singleDrumConf = Config.SingleDrumConf.parseFrom(inp);
					callback.configRead(singleDrumConf);
					log("Got drum conf for pad " + singleDrumConf.getPadNr());

					break;
				case Config.Command.WriteGlobalConf_VALUE:
					Config.GlobalConf gConf = Config.GlobalConf.parseFrom(inp);
					globalConf.mergeFrom(gConf);
					callback.globalConfRead(globalConf.build());
					break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	Config.GlobalConf.Builder globalConf = Config.GlobalConf.newBuilder();
}
