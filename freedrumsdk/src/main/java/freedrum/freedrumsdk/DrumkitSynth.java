package freedrum.freedrumsdk;

/**
 * Created by august on 7/2/17.
 */

public class DrumkitSynth {

	public native boolean loadDrumkit(String sfzfile);
	public native void playNote(int note, int velocity);

	public native void initAudio(int samplerate, int buffersize );
}
