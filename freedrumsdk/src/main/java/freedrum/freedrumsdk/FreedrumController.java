package freedrum.freedrumsdk;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.ParcelUuid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by august on 4/7/17.
 */

public class FreedrumController {
    private boolean mScanning;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner bluetoothLeScanner;
    private Activity context;
    private DrumstickFoundCallback callback;

    static private FreedrumController inst;
    public static FreedrumController instance(Activity activity){
        if (inst==null)
            inst=new FreedrumController(activity);
        if (activity!=null)
            inst.context=activity;
        return inst;
    }

    public FreedrumController()
    {}

    private FreedrumController(Activity context) {
        this.context = context;

        final BluetoothManager bluetoothManager =
                (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        bluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();

    }

    public void stopScan() {
        bluetoothLeScanner.stopScan(scanCallback);

    }

    final ScanCallback scanCallback = new ScanCallback() {

        @Override
        public void onScanFailed(int errorCode) {
            switch (errorCode){
                case ScanCallback.SCAN_FAILED_ALREADY_STARTED:

                break;
            }
            callback.scanFailed(errorCode);
        }

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            ScanRecord scanRecord = result.getScanRecord();

//                List<ParcelUuid> serviceUuids = scanRecord.getServiceUuids();

            BluetoothDevice device = result.getDevice();
            Drumstick drumstick;
            if (!deviceList.contains(device)) {
                deviceList.add(device);
                drumstick = new Drumstick(device, context);
                callback.stickFound(drumstick);
                drumsticks.put(device.getAddress(),drumstick);
            }
            else
                drumstick=drumsticks.get(device.getAddress());

            drumstick.rssiVal=result.getRssi();
            drumstick.rssiTime=System.currentTimeMillis();
        }
    };

    /**
     * start scan
     *
     * @param callback
     * @return 0 if scanning was started. -1 if we had no permission to do it.
     */
    public int startScan(final DrumstickFoundCallback callback) {

        deviceList.clear();
        drumsticks.clear();

        this.callback = callback;
        String coarseLocation = "android.permission.ACCESS_COARSE_LOCATION";
        if (context.checkSelfPermission(coarseLocation) != PackageManager.PERMISSION_GRANTED) {
            final String[] permissions = {coarseLocation};
            context.requestPermissions(permissions, 1);
            return -1;
        }


        mScanning = true;


        ScanFilter scanFilter = new ScanFilter.Builder().setServiceUuid(freedrum_service__uuid).build();
        //bluetoothLeScanner.startScan(scanCallback);
        bluetoothLeScanner.startScan(Arrays.asList(scanFilter), new ScanSettings.Builder().build(), scanCallback);
        return 0;
    }

    public Drumstick getDrumstick(String address,DrumstickCallback callback) {
        Drumstick drumstick = drumsticks.get(address);
        if (drumstick==null) {

            BluetoothDevice remoteDevice = mBluetoothAdapter.getRemoteDevice(address);
            deviceList.add(remoteDevice);
            drumstick = new Drumstick(remoteDevice, context);
            drumsticks.put(address, drumstick);
        }
        drumstick.callback=callback;
        return drumstick;
    }

    List<BluetoothDevice> deviceList = new ArrayList<>();
    HashMap<String,Drumstick> drumsticks=new HashMap<>();

    private static final long SCAN_PERIOD = 1000 * 60;
    private Handler mHandler;


	final static ParcelUuid apple_midi_service_uuid = ParcelUuid.fromString("03b80e5a-ede8-4b33-a751-6ce34ec4c700");
	static ParcelUuid freedrum_service__uuid = ParcelUuid.fromString("0e5a1523-ede8-4b33-a751-6ce34ec47c00");
	final static ParcelUuid freedrum_char_orientation = ParcelUuid.fromString("0e5a1525-ede8-4b33-a751-6ce34ec47c00");
	final static ParcelUuid freedrum_drum_conf = ParcelUuid.fromString("0e5a1526-ede8-4b33-a751-6ce34ec47c00");
	final static ParcelUuid freedrum_global_conf = ParcelUuid.fromString("0e5a1527-ede8-4b33-a751-6ce34ec47c00");
	final static ParcelUuid freedrum_status_uuid = ParcelUuid.fromString("0e5a1528-ede8-4b33-a751-6ce34ec47c00");

	final static ParcelUuid apple_midi_io_char=ParcelUuid.fromString("7772e5db-3868-4112-a1a9-f2669d106bf3");
}
